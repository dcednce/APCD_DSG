Effective March 1, 2020 - Version 4
-----------------------------------

**Visual Change Diff**

HTML-based visual change diff [available here](https://gitlab.com/UtahOHCS/APCD_DSG/-/raw/master/APCD_UT_DSG_40-from-311.diff.html?inline=false).

Scroll to the far right to see changes in Utah DSG 4.0 context.

Scroll to the far left to see changes in Utah DSG 3.1.1 context.

**General Changes**
- Removed "required" element column from format tables and incorporated specific requirements in element descriptions.
- Revised "Required Element" language in instructions.
- Replaced large lookup tables with reference to national standard.
- Incorporated language from the December 2016 "Technical Guidance - Rolling Three Month Eligibility Files"
- Added table of contents.
- Minor reorganization.
- Corrected minor typographical errors.
- Added EPO (Exclusive Provider Organization) to insurance product type table.

**Eligibility File Changes**
- Added 5 empty fields to eligibility table.
  - ME990
  - ME991
  - ME992
  - ME993
  - ME994

**Medical Claims File Changes**
- Added "In Plan Network Indicator" field to medical claims table.
  - MC916
- Added "Allowed Amount" field to medical claims table.
  - MC955
- Added 5 empty fields to medical claims table.
  - MC990
  - MC991
  - MC992
  - MC993
  - MC994 
- Added "42 CRF Part 2 Flag" to medical claims table.
  - MC999

**Pharmacy Claims File Changes**
- Added "In Plan Network Indicator" field to pharmacy claims table.
  - PC915
- Added "Allowed Amount" field to pharmacy claims table.
  - PC907
- Changed PC043 from "unassigned" to "Pharmaceutical Company Rebate".
  - PC043
- Added 5 empty fields to pharmacy claims table.
  - PC990
  - PC991
  - PC992
  - PC993
  - PC994
- Added "42 CRF Part 2 Flag" to pharmacy claims table.
  - PC999
- Added "Prescription Number" to pharmacy claims table.
  - PC906
- Changed PC034 to length of 5 to accommodate payer source data.

**Provider File Changes**
- Added 5 empty fields to provider table.
  - MP990
  - MP991
  - MP992
  - MP993
  - MP994

2018-03-15 - Version 3.1.1 (AKA 3.1 "CORRECTED")
------------------------------------------------

2017-03-01 - Version 3.0
------------------------

2016-03-01 - Version 2.2
------------------------

2015-04-01 - Version 2.1
------------------------

2014-05-01 - Version 2.0
------------------------
# Utah All-Payer Claims Database (APCD) Data Submission Guide

Version 4

## Table of Contents

- [Revision History](#revision-history)
- [General Data Submission Requirements](#general-data-submission-requirements)
  - [Data to be Submitted](#data-to-be-submitted)
  - [Coordination of Submissions](#coordination-of-submissions)
  - [File Submission Methods](#file-submission-methods)
  - [Data Quality Requirements](#data-quality-requirements)
  - [File Format](#file-format)
  - [Data Element Types](#data-element-types)
- [Exhibit A Data Elements](#exhibit-a-data-elements)
  - [A-1 Member Eligibility for Claims Data](#a-1-member-eligibility-for-claims-data)
    - [A-1.1 Member Eligibility File Layout](#a-11-member-eligibility-file-layout)
  - [A-2 Medical/Dental Claims Data](#a-2-medical-claims-data)
    - [A-2.1 Medical/Dental Claims File Layout](#a-21-medicaldental-claims-file-layout)
  - [A-3 Pharmacy Claims Data](#a-3-pharmacy-claims-data)
    - [A-3.1 Pharmacy Claims File Layout](#a-31-pharmacy-claims-file-layout)
  - [A-4 Provider Data](#a-4-provider-data)
    - [A-4.1 Provider File Layout](#a-41-provider-file-layout)
- [B-1 Lookup Tables](#b-1-lookup-tables)
  - [B-1.A Insurance Type](#b-1a-insurance-type)
  - [B-1.B Coverage Level Code](#b-1b-coverage-level-code)
  - [B-1.C Relationship Codes](#b-1c-relationship-codes)
  - [B-1.D Race Codes](#b-1d-race-codes)
  - [B-1.E Ethnicity Codes](#b-1e-ethnicity-codes)
  - [B-1.F Discharge Status](#b-1f-discharge-status)
  - [B-1.G Type of Bill](#b-1g-type-of-bill)
  - [B-1.H Claim Status](#b-1h-claim-status)
  - [B-1.I Present on Admission Codes](#b-1i-present-on-admission-codes)
  - [B-1.J Dispense as Written Codes](#b-1j-dispense-as-written-codes)
  - [B-1.K Drug Unit of Measure](#b-1k-drug-unit-of-measure)

## Revision History

|     Date      |  Version  |                  Description                  |      Author(s)       |
| ------------- | --------- | --------------------------------------------- | -------------------- |
| Oct 2013      | A         | Initial draft                                 | S. Murphy            |
| Oct 2013      | B         | Changes based on payer comments               | C. Hawley            |
| Sept 2014     | 2.1       | Incorporated changes approved by HDC          | C. Hawley            |
| Sept 2015     | 2.2       | Incorporated changes approved by HDC          | C. Hawley            |
| July 2016     | 3         | Incorporated changes approved by HDC          | C. Hawley            |
| July 2017     | 3.1       | Proposed Changes                              | C. Hawley            |
| March 2018    | 3.1       | Correction                                    | C. Hawley            |
| April 2019    | 3.2 DRAFT | Proposed Changes                              | S. Petersen/B. Scott |
| May 2019      | 3.2 DRAFT | Changes based on feedback recevied during PTF | S. Petersen          |
| August 2019   | 4         | Finalized Version 4                           | S. Petersen/B. Scott |
| January 2020  | 4         | Minor corrections based on rule review        | S. Petersen          |
| February 2020 | 4         | Minor, non-substantive corrections            | S. Petersen          |

## General Data Submission Requirements

Data submissions detailed below will include eligibility, medical/dental claims,
pharmacy claims, and provider data. Field definitions and other relevant data
associated with these submissions are specified in [Exhibit
A](#exhibit-a-data-elements). This specification is based on recommendations
from the All Payer Claims Databased (APCD) Council developed in collaboration
with stakeholders across the nation.

### Data to be Submitted

1. Member Eligibility

    - Payers must provide a data set that contains information on every covered
      plan member who is a Utah resident whether
      or not the member utilized services during the reporting period. The file
      must include member identifiers, subscriber name and identifier, member
      relationship to subscriber, residence, age, race, ethnicity, and other
      required fields to allow retrieval of related information from pharmacy
      and medical/dental claims data sets ([Exhibit A](#exhibit-a-data-elements)).
    - A Utah resident is defined as any eligible member whose residence is
      within the State of Utah, and all covered dependents. An exception to this
      is subscribers covered under a student plan. In this case, any student
      enrolled in a student plan for a Utah college/university would be
      considered a Utah resident regardless of their address of record.
    - If dual coverage exists, send coverage of eligible members where payer
      insurance is primary or tertiary. ME028 is a flag to indicate whether this
      insurance is primary or tertiary coverage.

2. Medical/Dental Claims

    - Payers shall report health care service paid claims and encounters for all
      Utah resident members (see requirements and definitions above). Payers may
      be required to identify encounters corresponding to a capitated payment ([Exhibit
      A-2](#a-2-medical-claims-data)).
    - Payers must provide information to identify the type of service and
      setting in which the service was provided.
    - Claim data is required for submission for each month during which some
      action has been taken on that claim (i.e. payment, adjustment or other
      modification). Any claims that have been "soft" denied (denied for
      incompleteness, incorrect or other administrative reasons) which the data
      supplier expects to be resubmitted upon correction, do not have to be
      submitted until corrections have been completed and the claim paid. It is
      desirable that payers provide a reference that links the original claim to
      all subsequent actions associated with that claim (see [Exhibit
      A-2](#a-2-medical-claims-data) for specifics).
    - International Classification of Diseases, Tenth Revision, Clinical
      Modification (ICD-10-CM) and ICD-10, Procedure Coding System (ICD-10-PCS)
      are required to accurately report patients’ risk factors. Healthcare
      Common Procedural Coding System (HCPCS) and Current Procedural Terminology
      (CPT) codes are also required.
    - Stand-alone dental carriers should provide contact information to OHCS as
      required by Utah Administrative Code and submit claims in compliance with
      this manual.

3. Pharmacy Claims

    - Payers must provide data for all paid pharmacy claims for prescriptions
      that were dispensed to members during the reporting period ([Exhibit
      A-3](#a-3-pharmacy-claims-data)).
    - If your health plan allows for medical coverage without pharmacy (or vice
      versa), ME018 — ME020 in [Exhibit
      A-1](#a-1-member-eligibility-for-claims-data) provides data elements which
      must accurately represent a member’s coverage.

4. Providers

    - Payers must provide a data set that contains information on every health
      care provider for whom claims were adjudicated during the reporting
      period.
    - In the event the same health care provider delivered and was reimbursed
      for services rendered from two different physical locations, then the
      provider data file shall contain two separate records for that same
      provider reflecting each of those physical locations. One record shall be
      provided for each unique physical location for a provider.

### Coordination of Submissions

In the event that the health plan contracts with a pharmacy benefits manager or
other service entity that manages claims for Utah residents, the health plan
shall be responsible for ensuring that complete and accurate files are submitted
to the APCD by the subcontractor. The health plan shall ensure that the member
identification information on the subcontractor’s file(s) is consistent with the
member identification information on the health plan’s eligibility, medical
claims and dental claims files. The health plan shall include utilization and
cost information for all services provided to members under any financial
arrangement.

### File Submission Methods

1. SFTP

    Secure File Transport Protocol involves logging on to the appropriate FTP
    site and sending or receiving files using the SFTP client.

2. Web Upload

    This method allows the sending and receiving of files and messages without
    the installation of additional software. This method requires internet
    access, a username and password.

### Data Quality Requirements

1. Required Elements

    Exhibit A provides a listing of data elements that are required. A data
    element that is required must contain a valid value unless a waiver is put
    in place with a specific payer who is unable to provide that data element
    due to system limitations. A “valid value” means that a percentage of all
    records have a value in the field based on the expected frequency this data
    element is available. Data files that do not achieve this threshold
    percentage for that data element may be rejected or require follow up prior
    to load into the APCD.

2. Validation and Quality Checks

    Data validation and quality edits will be developed in collaboration with
    each payer and refined as test data and production data is brought into the
    APCD. Data files missing required fields or containing mismatched claim
    line/record line totals may be rejected on submission. Other data elements
    will be validated against established ranges as the database is populated
    and may require manual intervention in order to ensure the data is correct.

    The objective is to populate the APCD with quality data and each payer will
    need to work interactively with the Utah Department of Health (UDOH), Office
    of Heath Care Statistics (OHCS) to develop data extracts that achieve
    validation and quality specifications. Waivers may be granted, at the
    discretion of OHCS, for data variances that cannot be corrected due to
    systematic issues that require substantial effort to correct.

### File Format

1. Standards for Text Files

    - Always one line item per row; No single line item of data may contain
      carriage return or line feed characters.
    - All rows delimited by the carriage return + line feed character
      combination.
    - All fields are variable field length, delimited using the pipe character
      (ASCII=124). It is imperative that no pipes (`|`) appear in the data
      itself. If your data contains pipes, either remove them or discuss using
      an alternate delimiter character.
    - Text fields are never demarcated or enclosed in single or double quotes.
      Any quotes detected are regarded as a part of the actual data.
    - The first row always contains the names of data columns.
    - Unless otherwise stipulated, numbers (e.g. ID numbers, account numbers) do
      not contain spaces, hyphens or other punctuation marks.
    - Text fields are never padded with leading or trailing spaces or tabs.
    - Numeric fields are never padded with leading or trailing zeros.
    - If a field is not available, or is not applicable, leave it blank. 'Blank'
      means do not supply any value at all between pipes (including quotes or
      other characters).

2. File Naming Convention

    All files submitted to the APCD shall have a naming convention developed to
    facilitate file management without requiring access to the contents. All
    files names will follow the template:

    ``` txt
    UTAPCD_PayerID_TestorProd_EntityAbreviation_SubmisionDate_CoveragePeriodDate.txt
    ```

    ``PayerID`` – This is the payer ID assigned to each submitter

    ``TestorProd`` – Test for test files; Prod for production

    ``EntityAbbreviation`` – ME, MC, PC, MP (ME – Member Eligibility, MC –
    Medical/Dental Claims, PC – Pharmacy Claims, MP – Medical/Dental Provider)

    ``SubmissionDate`` – Date File was produced. This date should be in the
    YYYYMMDD format.

    ``CoveragePeriodDate`` – The coverage period for the transmission. This date
    should be in the YYYYMMDD format.

### Data Element Types

``date`` – date data type for dates from 1/1/0001 through 12/31/9999

``int`` – integer (whole number)

``decimal/numeric`` – fixed precision and scale numeric data

``char`` – fixed length non-unicode data with a max of 8,000 characters

``varchar`` – variable length non-unicode data with a maximum of 8,000
characters

``text`` – variable length non-unicode data with a maximum of 2^31 -1 characters

## Exhibit A Data Elements

### A-1 Member Eligibility for Claims Data

_Frequency: Monthly Upload via FTP or Web Portal_

It is critical that the member ID (Member Suffix or Sequence Number) is unique
to an individual and that this unique identifier in the eligibility file is
consistent with the unique identifier in the medical/dental claims and pharmacy claims
file. Additional formatting requirements:

- One record, per member, per month, per insurance type, is required. For
  example, if a member is covered as both a subscriber and a dependent on two
  different policies during the same month, two records must be submitted. If a
  member has two contract numbers for two different coverage types, two member
  eligibility records must be submitted.
- In order to accurately capture eligibility end dates, payers will submit the
  previous three months eligibility monthly. This will provide run out to ensure
  ME005B is populated with a valid last day of eligibility for all members
  during the previous three months.
- If a carrier has submitted an eligibility record for a person, and later
  learns that the member was never eligible for one of the months submitted
  previously, in a subsequent submission, a record should be submitted for the
  person with:
  - the eligibility year (ME004) and eligibility month (ME005) set to the month in
  question;
  - eligibilty start and end day variables (ME005A and ME005B) set to '0';
  - medical, prescription drug, and dental coverage indicators (ME018-ME020)
      set to 'N'.
- Payers submit data in a single consistent format for each data type.

#### A-1.1 Member Eligibility File Layout

| DSG # | Data Element # |              Data Element Name               |  Type   | Length |                                                                                                                                                                                                                                                         Description/Codes/Sources                                                                                                                                                                                                                                                         |
| ----- | -------------- | -------------------------------------------- | ------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1     | ME001          | Payer Code                                   | varchar | 8      | Distributed by OHCS                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| 2     | ME002          | Payer Name                                   | varchar | 30     | Distributed by OHCS                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| 3     | ME003          | Insurance Type Code/Product                  | char    | 2      | See [Lookup Table B-1.A](#b-1a-insurance-type)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| 4     | ME004          | Year                                         | int     | 4      | 4 digit Year for which eligibility is reported in this submission                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| 5     | ME005          | Month                                        | char    | 2      | Month for which eligibility is reported in this submission expressed numerical from 01 to 12.                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| 6     | ME006          | Insured Group or Policy Number               | varchar | 30     | Group or policy number - not the number that uniquely identifies the subscriber.<br>  Medicaid Fee for Service will populate this field with the Aid Category Code.                                                                                                                                                                                                                                                                                                                                                                       |
| 7     | ME007          | Coverage Level Code                          | char    | 3      | Benefit coverage level. See [Lookup Table B-1.B](#b-1b-coverage-level-code)                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| 8     | ME008          | Subscriber Social Security Number            | varchar | 9      | Subscriber’s Social Security Number; Leave blank if unavailable                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| 9     | ME009          | Plan Specific Contract Number                | varchar | 128    | Plan assigned subscriber’s contract number; Leave blank if contract number = subscriber’s Social Security Number or use an alternate unique identifier such as Medicaid ID. Must be an identifier that is unique to the subscriber.                                                                                                                                                                                                                                                                                                       |
| 10    | ME010          | Member Suffix or Sequence Number             | varchar | 128    | Unique number of the member.  This column is the unique identifying column for membership and related medical and pharmacy claims. Only one record per eligibility month.  Must match MC009 and PC009.                                                                                                                                                                                                                                                                                                                                    |
| 11    | ME011          | Member Identification Code                   | varchar | 9      | Member’s Social Security Number; Leave blank if unavailable.                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| 12    | ME012          | Individual Relationship Code                 | char    | 2      | Member's relationship to insured – see [Lookup Table B-1.C](#b-1c-relationship-codes)                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| 13    | ME013          | Member Gender                                | char    | 1      | M – Male <br>F – Female <br>U - UNKNOWN                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| 14    | ME014          | Member Date of Birth                         | date    | 8      | YYYYMMDD                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| 15    | ME015          | Member City Name                             | varchar | 30     | City location of member                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| 16    | ME016          | Member State or Province                     | char    | 2      | As defined by the US Postal Service                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| 17    | ME017          | Member ZIP Code                              | varchar | 11     | ZIP Code of member - may include non-US codes. Do not include dash. Plus 4 optional but desired.                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| 18    | ME018          | Medical Coverage                             | char    | 1      | Y – YES <br>N - NO <br>3 - UNKNOWN                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| 19    | ME019          | Prescription Drug Coverage                   | char    | 1      | Y – YES <br>N - NO <br>3 - UNKNOWN                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| 20    | ME020          | Dental Coverage                              | char    | 1      | Y – YES <br>N – NO <br>3 - UNKNOWN                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| 21    | ME021          | Race 1                                       | varchar | 6      | See [Lookup Table B-1.D](#b-1d-race-codes)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| 22    | ME022          | Race 2                                       | varchar | 6      | See [Lookup Table B-1.D](#b-1d-race-codes)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| 23    | ME023          | Other Race                                   | varchar | 15     | List race if MC021 or MC022 are coded as R9.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| 24    | ME024          | Hispanic Indicator                           | char    | 1      | Y = Patient is Hispanic/Latino/Spanish <br>N = Patient is not Hispanic/Latino/Spanish <br>U = Unknown                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| 25    | ME025          | Ethnicity 1                                  | varchar | 6      | See [Lookup Table B-1.E](#b-1e-ethnicity-codes)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| 26    | ME026          | Ethnicity 2                                  | varchar | 6      | See code set for ME025.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| 27    | ME027          | Other Ethnicity                              | varchar | 20     | List ethnicity if MC025 or MC026 are coded as OTHER.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| 28    | ME028          | Primary Insurance Indicator                  | char    | 1      | Y – Yes, primary insurance <br>N – No, secondary or tertiary insurance                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| 29    | ME029          | Coverage Type                                | char    | 3      | STN – short-term, non-renewable health insurance (ie COBRA) <br>UND – plans underwritten by the insurer <br>OTH – any other plan. Insurers using this code shall obtain prior approval. <br>AWS – Self-funded                                                                                                                                                                                                                                                                                                                             |
| 30    | ME030          | Market Category Code                         | varchar | 4      | IND – policies sold and issued directly to individuals (non-group) <br>FCH – policies sold and issued directly to individuals on a franchise basis <br>GS3 – policies sold and issued directly to employers having 50 or more employees <br>GSA – policies sold and issued directly to small employers through a qualified association trust <br>OTH – policies sold to other types of entities. Insurers using this market code shall obtain prior approval.                                                                             |
| 31    | ME032          | Group Name                                   | varchar | 128    | Group name or IND for individual policies                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| 32    | ME043          | Member Street Address                        | varchar | 50     | Street address of member                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| 33    | ME044          | Employer Name                                | varchar | 50     | Name of the Employer, or if same as Group Name, leave blank                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| 34    | ME101          | Subscriber Last Name                         | varchar | 128    | The subscriber last name                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| 35    | ME102          | Subscriber First Name                        | varchar | 128    | The subscriber first name                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| 36    | ME103          | Subscriber Middle Initial                    | char    | 1      | The subscriber middle initial                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| 37    | ME104          | Member Last Name                             | varchar | 128    | The member last name                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| 38    | ME105          | Member First Name                            | varchar | 128    | The member first name                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| 39    | ME897          | Plan Effective Date                          | date    | 8      | YYYYMMDD Date eligibility started for this member under this plan type. The purpose of this data element is to maintain eligibility span for each member.                                                                                                                                                                                                                                                                                                                                                                                 |
| 40    | ME045          | Exchange Offering                            | char    | 1      | Identifies whether or not a policy was purchased through the Utah Health Benefits Exchange (UBHE).  <br>Y=Commercial small or non-group QHP purchased through the Exchange <br>N=Commercial small or non-group QHP purchased outside the Exchange <br>U= Not applicable (plan/product is not offered in the commercial small or non-group market)                                                                                                                                                                                         |
| 41    | ME106          | Group Size                                   | char    | 2      | Code indicating Group Size consistent with Utah Insurance Law and Regulation <br>A – 1 <br>B – 2 to 50 <br>C – 51 – 100 <br>D – 100+  <br>Required only for plans sold in the commercial large, small and non-group markets.  The following plan/products do not need to report this value: <br>Student plans <br>Medicare supplemental <br>Medicaid-funded plans <br>Stand-alone behavioral health, dental and vision                                                                                                                    |
| 42    | ME107          | Risk Basis                                   | char    | 1      | S – Self-insured <br>F – Fully insured                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| 43    | ME108          | High Deductible/ Health Savings Account Plan | char    | 1      | Y – Plan is High Deductible/HSA eligible <br>N – Plan is not High Deductible/HSA eligible                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| 44    | ME120          | Actuarial Value                              | decimal | 6      | Report value as calculated in the most recent version of the HHS Actuarial Value Calculator available at http://cciio.cms.gov/resources/regulations/index.html  Size includes decimal point.  Required as of January 1, 2014 for small group and non-group (individual) plans sold inside or outside the Exchange. <br>Required if ME106 IN ('A','B'), optional otherwise.                                                                                                                                                                |
| 45    | ME121          | Metallic Value                               | int     | 1      | Metal Level (percentage of Actuarial Value) per federal regulations. Valid values are: <br>1 – Platinum <br>2--Gold <br>3 – Silver <br>4 – Bronze <br>5 – Catastrophic <br>0 – Not Applicable  <br>Required as of January 1, 2014 for small group and non-group (individual) plans sold inside or outside the Exchange.  Use values provided in the most recent version of the HHS Actuarial Value Calculator available at: http://cciio.cms.gov/resources/regulations/index.html <br>Required if ME106 IN ('A','B'), optional otherwise. |
| 46    | ME122          | Grandfather Status                           | char    | 1      | See definition of “grandfathered plans” in HHS rules CFR 147.140  <br>Y= Yes <br>N = No  <br>Required as of January 1, 2014 for small group and non-group (individual) plans sold inside or outside the Exchange. <br>Required if ME106 IN ('A','B'), optional otherwise.                                                                                                                                                                                                                                                                 |
| 47    | ME899          | Record Type                                  | char    | 2      | Value = ME                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| 48    | ME123          | HIOS SCID                                    | char    | 17     | HIOS Standard Component ID with CSR variant e.g. 12345UT0010001-00 where <br>12345 is the unique Issuer HIOS ID <br>UT is the state code for Utah <br>0010001 is Issuer defined and indicates a specific plan <br>-00 is the cost sharing variant such that <br>-00 off exchange <br>-01 on exchange <br>-02 zero cost sharing <br>-03 limited cost sharing <br>-04 73% AV Silver <br>-05 87% AV Silver <br>-06 94% AV Silver <br>Required if subject to ACA Risk Adjustment, optional otherwise.                                         |
| 49    | ME124          | ACA Rating Area                              | int     | 1      | Geographic rating areas associated with the plan premium. <br>Value = 1, 2, 3, 4, 5, or 6  <br>1 – Cache, Rich <br>2 – Box Elder, Morgan, Weber <br>3 – Davis, Salt Lake, Summit, Tooele, Wasatch <br>4 – Utah <br>5 – Iron, Washington <br>6 – Beaver, Carbon, Daggett, Duchesne, Emery, Garfield, Grand, Juab, Kane, Millard, Piute, San Juan, Sanpete, Sevier, Uintah, Wayne <br>Required if subject to ACA Risk Adjustment, optional otherwise.                                                                                       |
| 50    | ME125          | Subscriber Premium                           | int     | 10     | Monthly subscriber premium, include up to hundredths place, but do not code decimal point (e.g. for $1,123.58 input 112358). Only subscriber records should show a premium amount other than 0. Code as 0 for records where ME012 Individual Relationship Code is not “20 Employee/Self.” <br>Required if subject to ACA Risk Adjustment, optional otherwise.                                                                                                                                                                             |
| 51    | ME005A         | First day of eligibility in the month        | int     | 2      | Day in the month when eligibility began. The first day in the month the member was eligible.  Example: a member eligible for the entire month of February will have a value of 1.                                                                                                                                                                                                                                                                                                                                                         |
| 52    | ME005B         | Last day of eligibility in the month         | int     | 2      | Day in the month when eligibility ends. The last day in the month the member was eligible.  Example: a member eligible for the entire month of February will have a value of 28.                                                                                                                                                                                                                                                                                                                                                          |
| 53    | ME990          | Unassigned                                   |         |        | Reserved for future use.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| 54    | ME991          | Unassigned                                   |         |        | Reserved for future use.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| 55    | ME992          | Unassigned                                   |         |        | Reserved for future use.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| 56    | ME993          | Unassigned                                   |         |        | Reserved for future use.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| 57    | ME994          | Unassigned                                   |         |        | Reserved for future use.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |

### A-2 Medical/Dental Claims Data

_Frequency: Monthly Upload via FTP or Web Portal_

Additional formatting requirements:

- Claims are paid claims. Non-covered or denied claims (e.g. duplicate or
  patient ineligible claims) are not included.
  - It is assumed that a complete snapshot of the claim is submitted at the time
    of final payment.
  - All claim lines submitted are processed as a unit.
  - Modifications to any previously submitted claim are submitted one of two
    ways:
    - Reversals - reverse the entire original claim (using MC038) and a new
      claim may be submitted as a replacement, or
    - Update with new version - replace the original claim with a new version
      (using MC005A).
    - If a claim reversal is submitted in the same month as the original claim,
      submission of claims is unnecessary since neither were paid. However, if
      necessary in the payer system, the version (MC005A) shall be incremented
      to indicate the reversal (MC038) regardless of method used to modify
      previously submitted claims.
- Financial amount data elements (MC062-MC067) assume the following:
  - The sum of all claim lines for a given data element will equal the total
    charge, paid, prepaid, co-pay, coinsurance, or deductible amounts for the
    entire claim.
  - The paid amount provided for each non-charge financial amount data element
    is mutually exclusive.
- Payers submit data in a single consistent format for each data type.

#### A-2.1 Medical/Dental Claims File Layout

| DSG # | Data Element # |                      Data Element Name                       |  Type   | Length |                                                                                                                                                                                                          Description/Codes/Sources                                                                                                                                                                                                          |
| ----- | -------------- | ------------------------------------------------------------ | ------- | ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1     | MC001          | Payer Code                                                   | varchar | 8      | Distributed by OHCS                                                                                                                                                                                                                                                                                                                                                                                                                         |
| 2     | MC002          | Payer Name                                                   | varchar | 30     | Distributed by OHCS                                                                                                                                                                                                                                                                                                                                                                                                                         |
| 3     | MC003          | Insurance Type/Product Code                                  | char    | 2      | See [Lookup Table B-1.A](#b-1a-insurance-type)                                                                                                                                                                                                                                                                                                                                                                                              |
| 4     | MC004          | Payer Claim Control Number                                   | varchar | 35     | Must apply to the entire claim and be unique within the payer’s system.  No partial claims. Only paid or partially paid claims                                                                                                                                                                                                                                                                                                              |
| 5     | MC005          | Line Counter                                                 | int     | 4      | Line number for this service. The line counter begins with 1 and is incremented by 1 for each additional service line of a claim. All claims must contain a line 1.                                                                                                                                                                                                                                                                         |
| 6     | MC005A         | Version Number                                               | int     | 4      | The version number of this claim service line. The original claim will have a version number of 0, with the next version being assigned a 1, and each subsequent version being incremented by 1 for that service line. Plans that cannot increment this column may opt to use YYMM as the version number.                                                                                                                                   |
| 7     | MC006          | Insured Group or Policy Number                               | varchar | 30     | Group or policy number - not the number that uniquely identifies the subscriber.                                                                                                                                                                                                                                                                                                                                                            |
| 8     | MC007          | Subscriber Social Security Number                            | varchar | 9      | Subscriber’s Social Security Number; Leave blank if unavailable                                                                                                                                                                                                                                                                                                                                                                             |
| 9     | MC008          | Plan Specific Contract Number                                | varchar | 128    | Plan assigned subscriber’s contract number; Leave blank if contract number = subscriber’s Social Security Number or use an alternate unique identifier such as Medicaid ID. Must be an identifier that is unique to the subscriber.                                                                                                                                                                                                         |
| 10    | MC009          | Member Suffix or Sequence Number                             | varchar | 128    | Unique number of the member within the contract. Must be an identifier that is unique to the member.  Must match ME010.                                                                                                                                                                                                                                                                                                                     |
| 11    | MC010          | Member Identification Code (patient)                         | varchar | 9      | Member’s Social Security Number; Leave blank if unavailable.                                                                                                                                                                                                                                                                                                                                                                                |
| 12    | MC011          | Individual Relationship Code                                 | char    | 2      | Member's relationship to insured – payers will map their available codes to those listed in [Lookup Table B-1.C](#b-1c-relationship-codes).                                                                                                                                                                                                                                                                                                 |
| 13    | MC012          | Member Gender                                                | char    | 1      | M - Male <br>F - Female <br>U - Unknown                                                                                                                                                                                                                                                                                                                                                                                                     |
| 14    | MC013          | Member Date of Birth                                         | date    | 8      | YYYYMMDD                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| 15    | MC014          | Member City Name                                             | varchar | 30     | City name of member                                                                                                                                                                                                                                                                                                                                                                                                                         |
| 16    | MC107          | Member Street Address                                        | varchar | 50     | Physical street address of the covered member                                                                                                                                                                                                                                                                                                                                                                                               |
| 17    | MC015          | Member State or Province                                     | char    | 2      | As defined by the US Postal Service                                                                                                                                                                                                                                                                                                                                                                                                         |
| 18    | MC016          | Member ZIP Code                                              | varchar | 11     | ZIP Code of member - may include non-US codes. Plus 4 optional but desired.                                                                                                                                                                                                                                                                                                                                                                 |
| 19    | MC017          | Date Service Approved/Accounts Payable Date/Actual Paid Date | date    | 8      | YYYYMMDD                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| 20    | MC018          | Admission Date                                               | date    | 8      | YYYYMMDD. Required for institutional claims.                                                                                                                                                                                                                                                                                                                                                                                                |
| 21    | MC019          | Admission Hour                                               | char    | 4      | Time is expressed in military time - HHMM. Required for institutional claims.                                                                                                                                                                                                                                                                                                                                                               |
| 22    | MC020          | Admission Type                                               | int     | 1      | Required for institutional claims. SOURCE: National Uniform Billing Data Element Specifications                                                                                                                                                                                                                                                                                                                                             |
| 23    | MC021          | Admission Source                                             | char    | 1      | Required for institutional claims. SOURCE: National Uniform Billing Data Element Specifications                                                                                                                                                                                                                                                                                                                                             |
| 24    | MC022          | Discharge Hour                                               | int     | 4      | Time expressed in military time – HHMM. Required for institutional claims.                                                                                                                                                                                                                                                                                                                                                                  |
| 25    | MC023          | Discharge Status                                             | char    | 2      | Required for institutional claims. See [Lookup Table B-1.F](#b-1f-discharge-status)                                                                                                                                                                                                                                                                                                                                                         |
| 26    | MC024          | Service Provider Number                                      | varchar | 30     | Payer assigned service provider number. Submit facility for institutional claims; physician or healthcare professional for professional claims.  Must match MP001.                                                                                                                                                                                                                                                                          |
| 27    | MC025          | Service Provider Tax ID Number                               | varchar | 10     | Federal taxpayer's identification number                                                                                                                                                                                                                                                                                                                                                                                                    |
| 28    | MC026          | Service National Provider ID                                 | varchar | 20     | National Provider ID. This data element pertains to the entity or individual directly providing the service.                                                                                                                                                                                                                                                                                                                                |
| 29    | MC027          | Service Provider Entity Type Qualifier                       | char    | 1      | 1 Person <br>2 Non-Person Entity  <br>HIPAA provider taxonomy classifies provider groups (clinicians who bill as a group practice or under a corporate name, even if that group is composed of one provider) as a “person”, and these shall be coded as a person.                                                                                                                                                                           |
| 30    | MC916          | In Plan Network Indicator                                    | char    | 1      | Indicator flag specifying if the service is deemed "in network".<br>0 No<br>1 Yes<br>9 Unknown                                                                                                                                                                                                                                                                                                                                              |
| 31    | MC028          | Service Provider First Name                                  | varchar | 25     | Individual first name. Leave blank if provider is a facility or organization.                                                                                                                                                                                                                                                                                                                                                               |
| 32    | MC029          | Service Provider Middle Name                                 | varchar | 25     | Individual middle name or initial. Leave blank if provider is a facility or organization.                                                                                                                                                                                                                                                                                                                                                   |
| 33    | MC030          | Service Provider Last Name or Organization Name              | varchar | 60     | Full name of provider organization or last name of individual provider                                                                                                                                                                                                                                                                                                                                                                      |
| 34    | MC031          | Service Provider Suffix                                      | varchar | 10     | Suffix to individual name. Leave blank if provider is a facility or organization. The service provider suffix shall be used to capture the generation of the individual clinician (e.g., Jr., Sr., III), if applicable, rather than the clinician’s degree (e.g., MD, LCSW).                                                                                                                                                                |
| 35    | MC032          | Service Provider Specialty                                   | varchar | 50     | Report the HIPAA-compliant health care provider taxonomy code. Code set is freely available at the National Uniform Claims Committee’s web site at http://www.nucc.org/                                                                                                                                                                                                                                                                     |
| 36    | MC108          | Service Provider Street Address                              | varchar | 50     | Physical practice location street address of the provider administering the services                                                                                                                                                                                                                                                                                                                                                        |
| 37    | MC033          | Service Provider City Name                                   | varchar | 30     | Physical practice location city name                                                                                                                                                                                                                                                                                                                                                                                                        |
| 38    | MC034          | Service Provider State or Province                           | char    | 2      | As defined by the US Postal Service                                                                                                                                                                                                                                                                                                                                                                                                         |
| 39    | MC035          | Service Provider ZIP Code                                    | varchar | 11     | ZIP Code of provider - may include non-US codes; do not include dash. Plus 4 optional but desired.                                                                                                                                                                                                                                                                                                                                          |
| 40    | MC036          | Type of Bill – Institutional                                 | char    | 3      | See [Lookup Table B-1.G](#b-1g-type-of-bill). Required for institutional claims. Do not use for professional claims.                                                                                                                                                                                                                                                                                                                        |
| 41    | MC037          | Facility Type - Professional                                 | char    | 2      | Use CMS Place of Service Codes for Professional Claims. ADA Dental Claim Form Completion Instructions requests the same codes for Place of Treatment.  Required for professional and dental claims. Do not use for institutional claims.                                                                                                                                                                                                    |
| 42    | MC038          | Claim Status                                                 | char    | 2      | See [Lookup Table B-1.H](#b-1h-claim-status)                                                                                                                                                                                                                                                                                                                                                                                                |
| 43    | MC039          | Admitting Diagnosis                                          | varchar | 7      | ICD-10-CM. Do not code decimal point. Required for institutional claims.                                                                                                                                                                                                                                                                                                                                                                    |
| 44    | MC898          | ICD-9 / ICD-10 Flag                                          | char    | 1      | 0 - This claim contains ICD-9-CM codes <br>1 - This claim contains ICD-10-CM and ICD-10-PCS codes                                                                                                                                                                                                                                                                                                                                           |
| 45    | MC040          | E-Code                                                       | varchar | 7      | Describes an injury, poisoning or adverse effect. Do not code decimal point.                                                                                                                                                                                                                                                                                                                                                                |
| 46    | MC041          | Principal Diagnosis                                          | varchar | 7      | ICD-10-CM. Do not code decimal point.                                                                                                                                                                                                                                                                                                                                                                                                       |
| 47    | MC042          | Other Diagnosis – 1                                          | varchar | 7      | ICD-10-CM. Do not code decimal point.                                                                                                                                                                                                                                                                                                                                                                                                       |
| 48    | MC043          | Other Diagnosis – 2                                          | varchar | 7      | ICD-10-CM. Do not code decimal point.                                                                                                                                                                                                                                                                                                                                                                                                       |
| 49    | MC044          | Other Diagnosis – 3                                          | varchar | 7      | ICD-10-CM. Do not code decimal point.                                                                                                                                                                                                                                                                                                                                                                                                       |
| 50    | MC045          | Other Diagnosis – 4                                          | varchar | 7      | ICD-10-CM. Do not code decimal point.                                                                                                                                                                                                                                                                                                                                                                                                       |
| 51    | MC046          | Other Diagnosis – 5                                          | varchar | 7      | ICD-10-CM. Do not code decimal point.                                                                                                                                                                                                                                                                                                                                                                                                       |
| 52    | MC047          | Other Diagnosis – 6                                          | varchar | 7      | ICD-10-CM. Do not code decimal point.                                                                                                                                                                                                                                                                                                                                                                                                       |
| 53    | MC048          | Other Diagnosis – 7                                          | varchar | 7      | ICD-10-CM. Do not code decimal point.                                                                                                                                                                                                                                                                                                                                                                                                       |
| 54    | MC049          | Other Diagnosis – 8                                          | varchar | 7      | ICD-10-CM. Do not code decimal point.                                                                                                                                                                                                                                                                                                                                                                                                       |
| 55    | MC050          | Other Diagnosis – 9                                          | varchar | 7      | ICD-10-CM. Do not code decimal point.                                                                                                                                                                                                                                                                                                                                                                                                       |
| 56    | MC051          | Other Diagnosis – 10                                         | varchar | 7      | ICD-10-CM. Do not code decimal point.                                                                                                                                                                                                                                                                                                                                                                                                       |
| 57    | MC052          | Other Diagnosis – 11                                         | varchar | 7      | ICD-10-CM. Do not code decimal point.                                                                                                                                                                                                                                                                                                                                                                                                       |
| 58    | MC053          | Other Diagnosis – 12                                         | varchar | 7      | ICD-10-CM. Do not code decimal point.                                                                                                                                                                                                                                                                                                                                                                                                       |
| 59    | MC054          | Revenue Code                                                 | char    | 10     | National Uniform Billing Committee Codes. Code using leading zeroes, left justified, and four digits. Required for institutional claims.                                                                                                                                                                                                                                                                                                    |
| 60    | MC055          | HCPCS/CPT Procedure Code                                     | varchar | 10     | Healthcare Common Procedural Coding System (HCPCS). This includes the CPT codes maintained by the American Medical Association.                                                                                                                                                                                                                                                                                                             |
| 61    | MC056          | Procedure Modifier – 1                                       | char    | 2      | Procedure modifier required when a modifier clarifies/improves the reporting accuracy of the associated procedure code (MC055).                                                                                                                                                                                                                                                                                                             |
| 62    | MC057          | Procedure Modifier – 2                                       | char    | 2      | Procedure modifier required when a modifier clarifies/improves the reporting accuracy of the associated procedure code (MC055).                                                                                                                                                                                                                                                                                                             |
| 63    | MC058          | ICD-10-PCS Procedure Code                                    | char    | 7      | Primary procedure code for this line of service. Do not code decimal point. Required for institutional claims. Leave blank if not an institutional claim.                                                                                                                                                                                                                                                                                   |
| 64    | MC059          | Date of Service – From                                       | date    | 8      | First date of service for this service line. YYYYMMDD                                                                                                                                                                                                                                                                                                                                                                                       |
| 65    | MC060          | Date of Service – Thru                                       | date    | 8      | Last date of service for this service line. YYYYMMDD                                                                                                                                                                                                                                                                                                                                                                                        |
| 66    | MC061          | Quantity                                                     | int     | 10     | Count of services performed.                                                                                                                                                                                                                                                                                                                                                                                                                |
| 67    | MC062          | Charge Amount                                                | int     | 10     | Do not code decimal point or provide any punctuation. For example, $1,000.00 converted to 100000. Same format for all financial data that follows.                                                                                                                                                                                                                                                                                          |
| 68    | MC063          | Plan Paid Amount                                             | int     | 10     | Set to zero for capitated claims. Do not code decimal point.                                                                                                                                                                                                                                                                                                                                                                                |
| 69    | MC064          | Prepaid Amount                                               | int     | 10     | For capitated services, the fee for service equivalent amount. Do not code decimal point.                                                                                                                                                                                                                                                                                                                                                   |
| 70    | MC065          | Co-pay Amount                                                | int     | 10     | The preset, fixed dollar amount for which the individual is responsible. Do not code decimal point.                                                                                                                                                                                                                                                                                                                                         |
| 71    | MC066          | Coinsurance Amount                                           | int     | 10     | The dollar amount an individual is responsible for – not the percentage. Do not code decimal point.                                                                                                                                                                                                                                                                                                                                         |
| 72    | MC067          | Deductible Amount                                            | int     | 10     | Do not code decimal point.                                                                                                                                                                                                                                                                                                                                                                                                                  |
| 73    | MC955          | Allowed Amount                                               | int     | 10     | The maximum amount a plan will pay for a covered health care service. May also be called “eligible expense,” “payment allowance,” or “negotiated rate.” Do not code decimal point.                                                                                                                                                                                                                                                          |
| 74    | MC068          | Patient Account/Control Number                               | varchar | 20     | Number assigned by hospital.                                                                                                                                                                                                                                                                                                                                                                                                                |
| 75    | MC069          | Discharge Date                                               | date    | 8      | Date patient discharged. YYYYMMDD. Required for institutional claims.                                                                                                                                                                                                                                                                                                                                                                       |
| 76    | MC070          | Service Provider Country Name                                | varchar | 30     | Code US for United States.                                                                                                                                                                                                                                                                                                                                                                                                                  |
| 77    | MC071          | DRG                                                          | varchar | 10     | Insurers and health care claims processors shall code using the CMS methodology when available. Precedence shall be given to DRGs transmitted from the hospital provider. When the CMS methodology for DRGs is not available, but the DRG system is used, the insurer shall format the DRG and the complexity level within the same field with an “A” prefix, and with a hyphen separating the DRG and the complexity level (e.g. AXXX-XX). |
| 78    | MC072          | DRG Version                                                  | char    | 2      | Version number of the grouper used                                                                                                                                                                                                                                                                                                                                                                                                          |
| 79    | MC073          | APC                                                          | char    | 4      | Insurers and health care claims processors shall code using the CMS methodology when available. Precedence shall be given to APCs transmitted from the health care provider.                                                                                                                                                                                                                                                                |
| 80    | MC074          | APC Version                                                  | char    | 2      | Version number of the grouper used                                                                                                                                                                                                                                                                                                                                                                                                          |
| 81    | MC075          | Drug Code                                                    | varchar | 11     | An NDC code used only when a medication is paid for as part of a medical claim.                                                                                                                                                                                                                                                                                                                                                             |
| 82    | MC076          | Billing Provider Number                                      | varchar | 30     | Payer assigned billing provider number. This number should be the identifier used by the payer for internal identification purposes, and does not routinely change.  Must match MP001.                                                                                                                                                                                                                                                      |
| 83    | MC077          | Billing Provider NPI                                         | varchar | 20     | National Provider ID                                                                                                                                                                                                                                                                                                                                                                                                                        |
| 84    | MC078          | Billing Provider Last Name or Organization Name              | varchar | 60     | Full name of provider billing organization or last name of individual billing provider.                                                                                                                                                                                                                                                                                                                                                     |
| 85    | MC101          | Subscriber Last Name                                         | varchar | 128    | Subscriber last name                                                                                                                                                                                                                                                                                                                                                                                                                        |
| 86    | MC102          | Subscriber First Name                                        | varchar | 128    | Subscriber first name                                                                                                                                                                                                                                                                                                                                                                                                                       |
| 87    | MC103          | Subscriber Middle Initial                                    | char    | 1      | Subscriber middle initial                                                                                                                                                                                                                                                                                                                                                                                                                   |
| 88    | MC104          | Member Last Name                                             | varchar | 128    | Last name of member                                                                                                                                                                                                                                                                                                                                                                                                                         |
| 89    | MC105          | Member First Name                                            | varchar | 128    | First name of member                                                                                                                                                                                                                                                                                                                                                                                                                        |
| 90    | MC106          | Member Middle Initial                                        | char    | 1      | Middle initial of member                                                                                                                                                                                                                                                                                                                                                                                                                    |
| 91    | MC201A         | Present on Admission – PDX                                   | varchar | 1      | Code indicating the presence of diagnosis at the time of admission  See [Lookup Table B-1.I](#b-1i-present-on-admission-codes) for valid values. Required if associated diagnosis code filled.                                                                                                                                                                                                                                              |
| 92    | MC201B         | Present on Admission – DX1                                   | varchar | 1      | Code indicating the presence of diagnosis at the time of admission  See [Lookup Table B-1.I](#b-1i-present-on-admission-codes) for valid values. Required if associated diagnosis code filled.                                                                                                                                                                                                                                              |
| 93    | MC201C         | Present on Admission – DX2                                   | varchar | 1      | Code indicating the presence of diagnosis at the time of admission  See [Lookup Table B-1.I](#b-1i-present-on-admission-codes) for valid values. Required if associated diagnosis code filled.                                                                                                                                                                                                                                              |
| 94    | MC201D         | Present on Admission – DX3                                   | varchar | 1      | Code indicating the presence of diagnosis at the time of admission  See [Lookup Table B-1.I](#b-1i-present-on-admission-codes) for valid values. Required if associated diagnosis code filled.                                                                                                                                                                                                                                              |
| 95    | MC201E         | Present on Admission – DX4                                   | varchar | 1      | Code indicating the presence of diagnosis at the time of admission  See [Lookup Table B-1.I](#b-1i-present-on-admission-codes) for valid values. Required if associated diagnosis code filled.                                                                                                                                                                                                                                              |
| 96    | MC201F         | Present on Admission – DX5                                   | varchar | 1      | Code indicating the presence of diagnosis at the time of admission  See [Lookup Table B-1.I](#b-1i-present-on-admission-codes) for valid values. Required if associated diagnosis code filled.                                                                                                                                                                                                                                              |
| 97    | MC201G         | Present on Admission – DX6                                   | varchar | 1      | Code indicating the presence of diagnosis at the time of admission  See [Lookup Table B-1.I](#b-1i-present-on-admission-codes) for valid values. Required if associated diagnosis code filled.                                                                                                                                                                                                                                              |
| 98    | MC201H         | Present on Admission – DX7                                   | varchar | 1      | Code indicating the presence of diagnosis at the time of admission  See [Lookup Table B-1.I](#b-1i-present-on-admission-codes) for valid values. Required if associated diagnosis code filled.                                                                                                                                                                                                                                              |
| 99    | MC201I         | Present on Admission – DX8                                   | varchar | 1      | Code indicating the presence of diagnosis at the time of admission  See [Lookup Table B-1.I](#b-1i-present-on-admission-codes) for valid values. Required if associated diagnosis code filled.                                                                                                                                                                                                                                              |
| 100   | MC201J         | Present on Admission – DX9                                   | varchar | 1      | Code indicating the presence of diagnosis at the time of admission  See [Lookup Table B-1.I](#b-1i-present-on-admission-codes) for valid values. Required if associated diagnosis code filled.                                                                                                                                                                                                                                              |
| 101   | MC201K         | Present on Admission – DX10                                  | varchar | 1      | Code indicating the presence of diagnosis at the time of admission  See [Lookup Table B-1.I](#b-1i-present-on-admission-codes) for valid values. Required if associated diagnosis code filled.                                                                                                                                                                                                                                              |
| 102   | MC201L         | Present on Admission – DX11                                  | varchar | 1      | Code indicating the presence of diagnosis at the time of admission  See [Lookup Table B-1.I](#b-1i-present-on-admission-codes) for valid values. Required if associated diagnosis code filled.                                                                                                                                                                                                                                              |
| 103   | MC201M         | Present on Admission – DX12                                  | varchar | 1      | Code indicating the presence of diagnosis at the time of admission  See [Lookup Table B-1.I](#b-1i-present-on-admission-codes) for valid values. Required if associated diagnosis code filled.                                                                                                                                                                                                                                              |
| 104   | MC202          | Tooth Number                                                 | char    | 2      | Tooth Number or Letter Identification. Only include one tooth per claim line. If a procedure was performed on multiple teeth, such as a bridge, include only the first in the span. Required for dental claims.                                                                                                                                                                                                                             |
| 105   | MC203          | Area of Oral Cavity                                          | char    | 2      | Area of Oral Cavity codes are maintained by the American Dental Association. Required for dental claims.                                                                                                                                                                                                                                                                                                                                    |
| 106   | MC204          | Tooth Surface                                                | char    | 10     | Tooth Surface Identification. Required for dental claims.                                                                                                                                                                                                                                                                                                                                                                                   |
| 107   | MC205          | ICD-10-PCS Procedure Date                                    | date    | 8      | Date MC058 was performed. YYYYDDMM. Required for institutional claims. Leave blank if not an institutional claim.                                                                                                                                                                                                                                                                                                                           |
| 108   | MC058A         | ICD-10-PCS Procedure Code                                    | char    | 7      | Secondary procedure code for this line of service. Do not code decimal point. Leave blank if not an institutional claim.                                                                                                                                                                                                                                                                                                                    |
| 109   | MC205A         | ICD-10-PCS Procedure Date                                    | date    | 8      | Date MC058A was performed. YYYYDDMM. Required for institutional claims. Leave blank if not an institutional claim.                                                                                                                                                                                                                                                                                                                          |
| 110   | MC058B         | ICD-10-PCS Procedure Code                                    | char    | 7      | Secondary procedure code for this line of service. Do not code decimal point. Leave blank if not an institutional claim.                                                                                                                                                                                                                                                                                                                    |
| 111   | MC205B         | ICD-10-PCS Procedure Date                                    | date    | 8      | Date MC058B was performed. YYYYDDMM. Required for institutional claims. Leave blank if not an institutional claim.                                                                                                                                                                                                                                                                                                                          |
| 112   | MC058C         | ICD-10-PCS Procedure Code                                    | char    | 7      | Secondary procedure code for this line of service. Do not code decimal point. Leave blank if not an institutional claim.                                                                                                                                                                                                                                                                                                                    |
| 113   | MC205C         | ICD-10-PCS Procedure Date                                    | date    | 8      | Date MC058C was performed. YYYYDDMM. Required for institutional claims. Leave blank if not an institutional claim.                                                                                                                                                                                                                                                                                                                          |
| 114   | MC058D         | ICD-10-PCS Procedure Code                                    | char    | 7      | Secondary procedure code for this line of service. Do not code decimal point. Leave blank if not an institutional claim.                                                                                                                                                                                                                                                                                                                    |
| 115   | MC205D         | ICD-10-PCS Procedure Date                                    | date    | 8      | Date MC058D was performed. YYYYDDMM. Required for institutional claims. Leave blank if not an institutional claim.                                                                                                                                                                                                                                                                                                                          |
| 116   | MC058E         | ICD-10-PCS Procedure Code                                    | char    | 7      | Secondary procedure code for this line of service. Do not code decimal point. Leave blank if not an institutional claim.                                                                                                                                                                                                                                                                                                                    |
| 117   | MC205E         | ICD-10-PCS Procedure Date                                    | date    | 8      | Date MC058E was performed. YYYYDDMM. Required for institutional claims. Leave blank if not an institutional claim.                                                                                                                                                                                                                                                                                                                          |
| 118   | MC206          | Capitated Service Indicator                                  | char    | 1      | Y – services are paid under a capitated arrangement <br>N – services are not paid under a capitated arrangement <br>U – unknown                                                                                                                                                                                                                                                                                                             |
| 119   | MC899          | Record Type                                                  | char    | 2      | Value = MC                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| 120   | MC061A         | Unit of Measure                                              | char    | 2      | Unit of measure for MC061.  Valid values are: <br>DA – Days <br>MJ – Minutes <br>UN – Units Other standard ANSI values may be used with prior approval from OHCS.                                                                                                                                                                                                                                                                           |
| 121   | MC901          | Procedure Modifier – 3                                       | char    | 2      | Procedure modifier required when a modifier clarifies/improves the reporting accuracy of the associated procedure code (MC055).                                                                                                                                                                                                                                                                                                             |
| 122   | MC902          | Procedure Modifier – 4                                       | char    | 2      | Procedure modifier required when a modifier clarifies/improves the reporting accuracy of the associated procedure code (MC055).                                                                                                                                                                                                                                                                                                             |
| 123   | MC990          | Unassigned                                                   |         |        | Reserved for future use.                                                                                                                                                                                                                                                                                                                                                                                                                    |
| 124   | MC991          | Unassigned                                                   |         |        | Reserved for future use.                                                                                                                                                                                                                                                                                                                                                                                                                    |
| 125   | MC992          | Unassigned                                                   |         |        | Reserved for future use.                                                                                                                                                                                                                                                                                                                                                                                                                    |
| 126   | MC993          | Unassigned                                                   |         |        | Reserved for future use.                                                                                                                                                                                                                                                                                                                                                                                                                    |
| 127   | MC994          | Unassigned                                                   |         |        | Reserved for future use.                                                                                                                                                                                                                                                                                                                                                                                                                    |
| 128   | MC999          | 42 CFR Part 2 Flag                                           | char    | 1      | 0 No, this claim does not contain data protected under 42 CFR Part 2<br>1 Yes, this claim does contain data protected under 42 CFR Part 2<br>9 Unknown                                                                                                                                                                                                                                                                                      |

### A-3 Pharmacy Claims Data

_Frequency: Monthly Upload via FTP or Web Portal_

Additional formatting requirements:

- Claims are paid claims. Non-covered or denied claims (e.g. duplicate or
  patient ineligible claims) are not included.
  - It is assumed that a complete snapshot of the claim is submitted at the time
    of final payment.
  - All claim lines submitted are processed as a unit.
  - Modifications to any previously submitted claim are submitted one of two
    ways:
    - Reversals - reverse the entire original claim (using PC025) and a new
      claim may be submitted as a replacement, or
    - Update with new version - replace the original claim with a new version
      (using PC201).
    - If a claim reversal is submitted in the same month as the original claim,
      submission of claims is unnecessary since neither were paid. However, if
      necessary in the payer system, the version (PC201) shall be incremented to
      indicate the reversal (MC025) regardless of method used to modify
      previously submitted claims.
- Financial amount data elements (PC035-PC042) assume the following:
  - The sum of all claim lines for a given data element will equal the total
    charge, paid, ingredient cost, postage, dispensing fee, co-pay, coinsurance,
    or deductible amounts for the entire claim.
  - The paid amount provided for each non-charge financial amount data element
    is mutually exclusive.
- A claim for a compound drug (PC031) should include a claim line for each
  ingredient in the drug.
- Payers submit data in a single consistent format for each data type.

#### A-3.1 Pharmacy Claims File Layout

| DSG # | Data Element # |         Data Element Name         |  Type   | Length |                                                                                                      Description/Codes/Sources                                                                                                      |
| ----- | -------------- | --------------------------------- | ------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1     | PC001          | Payer Code                        | varchar | 8      | Distributed by OHCS                                                                                                                                                                                                                 |
| 2     | PC002          | Payer Name                        | varchar | 30     | Distributed by OHCS                                                                                                                                                                                                                 |
| 3     | PC003          | Insurance Type/Product Code       | char    | 2      | See [Lookup table B-1.A](#b-1a-insurance-type)                                                                                                                                                                                      |
| 4     | PC004          | Payer Claim Control Number        | varchar | 35     | Must apply to the entire claim and be unique within the payer's system.                                                                                                                                                             |
| 5     | PC005          | Line Counter                      | int     | 4      | Line number for this service. The line counter begins with 1 and is incremented by 1 for each additional service line of a claim.                                                                                                   |
| 6     | PC006          | Insured Group Number              | varchar | 30     | Group or policy number - not the number that uniquely identifies the subscriber                                                                                                                                                     |
| 7     | PC007          | Subscriber Social Security Number | varchar | 9      | Subscriber’s Social Security Number; Leave blank if unavailable                                                                                                                                                                     |
| 8     | PC008          | Plan Specific Contract Number     | varchar | 128    | Plan assigned subscriber’s contract number; Leave blank if contract number = subscriber’s Social Security Number or use an alternate unique identifier such as Medicaid ID. Must be an identifier that is unique to the subscriber. |
| 9     | PC009          | Member Suffix or Sequence Number  | varchar | 128    | Unique number of the member within the contract. Must be an identifier that is unique to the member.  Must match ME010.                                                                                                             |
| 10    | PC010          | Member Identification Code        | varchar | 9      | Member’s social security number; Leave blank if contract number = subscriber’s Social Security Number or use an alternate unique identifier such as Medicaid ID. Must be an identifier that is unique to the member.                |
| 11    | PC011          | Individual Relationship Code      | char    | 2      | Member's relationship to insured  See [Lookup Table B-1.C](#b-1c-relationship-codes)                                                                                                                                                |
| 12    | PC012          | Member Gender                     | char    | 1      | M – Male <br>F – Female <br>U – UNKNOWN                                                                                                                                                                                             |
| 13    | PC013          | Member Date of Birth              | date    | 8      | YYYYMMDD                                                                                                                                                                                                                            |
| 14    | PC014          | Member City Name of Residence     | varchar | 50     | City name of member                                                                                                                                                                                                                 |
| 15    | PC015          | Member State or Province          | char    | 2      | As defined by the US Postal Service                                                                                                                                                                                                 |
| 16    | PC016          | Member ZIP Code                   | varchar | 11     | ZIP Code of member - may include non-US codes; Do not include dash. Plus 4 optional but desired.                                                                                                                                    |
| 17    | PC017          | Date Service Approved (AP Date)   | date    | 8      | YYYYMMDD – date claim paid if available, otherwise set to Date Prescription Filled                                                                                                                                                  |
| 18    | PC018          | Pharmacy Number                   | varchar | 30     | Payer assigned pharmacy number. AHFS number is acceptable.  Must match MP001.                                                                                                                                                       |
| 19    | PC019          | Pharmacy Tax ID Number            | varchar | 10     | Federal taxpayer's identification number coded with no punctuation (carriers that contract with outside PBM’s will not have this)                                                                                                   |
| 20    | PC915          | In Plan Network Indicator         | char    | 1      | Indicator flag specifying if the service is deemed "in network".<br>0 No<br>1 Yes<br>9 Unknown                                                                                                                                      |
| 21    | PC020          | Pharmacy Name                     | varchar | 50     | Name of pharmacy                                                                                                                                                                                                                    |
| 22    | PC021          | Pharmacy NPI                      | varchar | 20     | Pharmacy’s National Provider ID. This data element pertains to the entity or individual directly providing the service.                                                                                                             |
| 23    | PC048          | Pharmacy Location Street Address  | varchar | 30     | Street address of pharmacy                                                                                                                                                                                                          |
| 24    | PC022          | Pharmacy Location City            | varchar | 30     | City name of pharmacy - preferably pharmacy location (if mail order leave blank)                                                                                                                                                    |
| 25    | PC023          | Pharmacy Location State           | char    | 2      | As defined by the US Postal Service (if mail order leave blank)                                                                                                                                                                     |
| 26    | PC024          | Pharmacy ZIP Code                 | varchar | 10     | ZIP Code of pharmacy - may include non-US codes. Do not include dash. Plus 4 optional but desired (if mail order leave blank)                                                                                                       |
| 27    | PC024d         | Pharmacy Country Name             | varchar | 30     | Code US for United States                                                                                                                                                                                                           |
| 28    | PC025          | Claim Status                      | char    | 2      | See [Lookup Table B-1.H](#b-1h-claim-status).                                                                                                                                                                                       |
| 29    | PC026          | Drug Code                         | varchar | 11     | NDC Code                                                                                                                                                                                                                            |
| 30    | PC027          | Drug Name                         | varchar | 80     | Text name of drug                                                                                                                                                                                                                   |
| 31    | PC028          | New Prescription or Refill        | varchar | 2      | 01 New prescription <br>02 – 99 Refill Count                                                                                                                                                                                        |
| 32    | PC029          | Generic Drug Indicator            | char    | 2      | 01 - branded drug <br>02 - generic drug                                                                                                                                                                                             |
| 33    | PC030          | Dispense as Written Code          | char    | 1      | Payers able to map available codes to those below. See [Lookup Table B-1.J](#b-1j-dispense-as-written-codes)                                                                                                                        |
| 34    | PC031          | Compound Drug Indicator           | char    | 1      | N Non-compound drug <br>Y Compound drug <br>U Non-specified drug compound                                                                                                                                                           |
| 35    | PC032          | Date Prescription Filled          | date    | 8      | YYYYMMDD                                                                                                                                                                                                                            |
| 36    | PC033          | Quantity Dispensed                | int     | 10     | Number of metric units of medication dispensed                                                                                                                                                                                      |
| 37    | PC034          | Days Supply                       | int     | 5      | Estimated number of days the prescription will last                                                                                                                                                                                 |
| 38    | PC035          | Charge Amount                     | int     | 10     | Do not code decimal point or provide any punctuation.  For example, $1,000.00 converted to 100000. Same format for all financial data that follows.                                                                                 |
| 39    | PC036          | Paid Amount                       | int     | 10     | Includes all health plan payments and excludes all member payments. Do not code decimal point.                                                                                                                                      |
| 40    | PC037          | Ingredient Cost/List Price        | int     | 10     | Cost of the drug dispensed. Do not code decimal point.                                                                                                                                                                              |
| 41    | PC038          | Postage Amount Claimed            | int     | 10     | Do not code decimal point. Not typically captured.                                                                                                                                                                                  |
| 42    | PC039          | Dispensing Fee                    | int     | 10     | Do not code decimal point.                                                                                                                                                                                                          |
| 43    | PC040          | Co-pay Amount                     | int     | 10     | The preset, fixed dollar amount for which the individual is responsible. Do not code decimal point.                                                                                                                                 |
| 44    | PC041          | Coinsurance Amount                | int     | 10     | The dollar amount an individual is responsible for – not the percentage. Do not code decimal point.                                                                                                                                 |
| 45    | PC042          | Deductible Amount                 | int     | 10     | Do not code decimal point.                                                                                                                                                                                                          |
| 46    | PC907          | Allowed Amount                    | int     | 10     | The maximum amount a plan will pay for a covered prescription. May also be called “eligible expense,” “payment allowance,” or “negotiated rate.” Do not code decimal point.                                                         |
| 47    | PC043          | Pharmaceutical Company Rebate     | int     | 10     | Do not code decimal point.                                                                                                                                                                                                          |
| 48    | PC044          | Prescribing Physician First Name  | varchar | 25     | Physician first name.                                                                                                                                                                                                               |
| 49    | PC045          | Prescribing Physician Middle Name | varchar | 25     | Physician middle name or initial.                                                                                                                                                                                                   |
| 50    | PC046          | Prescribing Physician Last Name   | varchar | 60     | Physician last name.                                                                                                                                                                                                                |
| 51    | PC047          | Prescribing Physician NPI         | varchar | 20     | NPI number for prescribing physician                                                                                                                                                                                                |
| 52    | PC049          | Member Street Address             | varchar | 50     | Street address of member                                                                                                                                                                                                            |
| 53    | PC101          | Subscriber Last Name              | varchar | 128    | Subscriber Last Name                                                                                                                                                                                                                |
| 54    | PC102          | Subscriber First Name             | varchar | 128    | Subscriber First Name                                                                                                                                                                                                               |
| 55    | PC103          | Subscriber Middle Initial         | char    | 1      | Subscriber Middle Initial                                                                                                                                                                                                           |
| 56    | PC104          | Member Last Name                  | varchar | 128    | Member Last Name                                                                                                                                                                                                                    |
| 57    | PC105          | Member First Name                 | varchar | 128    | Member First Name                                                                                                                                                                                                                   |
| 58    | PC106          | Member Middle Initial             | char    | 1      | Member Middle Initial                                                                                                                                                                                                               |
| 59    | PC201          | Version Number                    | int     | 4      | The version number of this claim service line. The original claim will have a version number of 0, with the next version being assigned a 1, and each subsequent version being incremented by 1 for that service line.              |
| 60    | PC202          | Prescription Written Date         | date    | 8      | Date Prescription was written                                                                                                                                                                                                       |
| 61    | PC047a         | Prescribing Physician Provider ID | varchar | 30     | Provider ID for the prescribing physician  Must match MP001.                                                                                                                                                                        |
| 62    | PC047b         | Prescribing Physician DEA         | varchar | 20     | DEA number for prescribing physician                                                                                                                                                                                                |
| 63    | PC899          | Record Type                       | char    | 2      | PC                                                                                                                                                                                                                                  |
| 64    | PC905          | Drug Unit of Measure              | varchar | 3      | Report the code that defines the unit of measure for the drug dispensed in PC033  See [Lookup Table B-1.K](#b-1k-drug-unit-of-measure) for valid values.                                                                            |
| 65    | PC906          | Prescription Number               | varchar | 20     | Unique prescription identifier                                                                                                                                                                                                      |
| 66    | PC990          | Unassigned                        |         |        | Reserved for future use.                                                                                                                                                                                                            |
| 67    | PC991          | Unassigned                        |         |        | Reserved for future use.                                                                                                                                                                                                            |
| 68    | PC992          | Unassigned                        |         |        | Reserved for future use.                                                                                                                                                                                                            |
| 69    | PC993          | Unassigned                        |         |        | Reserved for future use.                                                                                                                                                                                                            |
| 70    | PC994          | Unassigned                        |         |        | Reserved for future use.                                                                                                                                                                                                            |
| 71    | PC999          | 42 CFR Part 2 Flag                | char    | 1      | 0 No, this claim does not contain data protected under 42 CFR Part 2<br>1 Yes, this claim does contain data protected under 42 CFR Part 2<br>9 Unknown                                                                              |

### A-4 Provider Data

_Frequency: Monthly Upload via FTP or Web Portal_

Additional formatting requirements:

- Payers submit data in a single consistent format for each data type.
- A provider means a health care facility, health care practitioner, health
  product manufacturer, health product vendor or pharmacy.
- A billing provider means a provider or other entity that submits claims to
  health care claims processors for health care services directly or provided to
  a subscriber or member by a service provider.
- A service provider means the provider who directly performed or provided a
  health care service to a subscriber of member.
- One record submitted for each provider for each unique physical address.

#### A-4.1 Provider File Layout

| DSG # | Data Element # |            Data Element Name            |  Type   | Length |                                                                                                                          Description/Codes/Sources                                                                                                                           |
| ----- | -------------- | --------------------------------------- | ------- | ------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1     | MP001          | Provider ID                             | varchar | 30     | Unique identified for the provider as assigned by the reporting entity  Must match MC024, MC076, PC018, or PC047a.                                                                                                                                                           |
| 2     | MP002          | Provider Tax ID                         | varchar | 10     | Tax ID of the provider. Do not code punctuation.                                                                                                                                                                                                                             |
| 3     | MP003          | Provider Entity                         | char    | 1      | F – Facility <br>G – Provider Group <br>I – Independent Practice Association <br>P - Practitioner                                                                                                                                                                            |
| 4     | MP004          | Provider First Name                     | varchar | 25     | Individual first name. Leave blank if provider is a facility or organization.                                                                                                                                                                                                |
| 5     | MP005          | Provider Middle Name or Initial         | varchar | 25     | Provider Middle Name or Initial                                                                                                                                                                                                                                              |
| 6     | MP006          | Provider Last Name or Organization Name | varchar | 60     | Full name of provider organization or last name of individual provider                                                                                                                                                                                                       |
| 7     | MP007          | Provider Suffix                         | varchar | 10     | Suffix to individual name. Leave blank if provider is a facility or organization. The service provider suffix shall be used to capture the generation of the individual clinician (e.g., Jr., Sr., III), if applicable, rather than the clinician’s degree (e.g., MD, LCSW). |
| 8     | MP008          | Provider Specialty                      | varchar | 50     | Report the HIPAA-compliant health care provider taxonomy code. Code set is freely available at the National Uniform Claims Committee’s web site at http://www.nucc.org/                                                                                                      |
| 9     | MP009          | Provider Office Street Address          | varchar | 50     | Physical address – address where provider delivers health care services                                                                                                                                                                                                      |
| 10    | MP010          | Provider Office City                    | varchar | 30     | Physical address – city where provider delivers health care services                                                                                                                                                                                                         |
| 11    | MP011          | Provider Office State                   | char    | 2      | Physical address – state where provider delivers health care services. As defined by the US Postal Service.                                                                                                                                                                  |
| 12    | MP012          | Provider Office ZIP                     | varchar | 11     | Physical address – ZIP where provider delivers health care services. May include non-US codes; do not include dash. Plus 4 optional but desired.                                                                                                                             |
| 13    | MP013          | Provider DEA Number                     | varchar | 12     | Provider DEA Number                                                                                                                                                                                                                                                          |
| 14    | MP014          | Provider NPI                            | varchar | 20     | Provider NPI                                                                                                                                                                                                                                                                 |
| 15    | MP015          | Provider State License Number           | varchar | 20     | Prefix with two-character state of licensure with no punctuation. Example UTLL12345                                                                                                                                                                                          |
| 16    | MP899          | Record Type                             | char    | 2      | MP                                                                                                                                                                                                                                                                           |
| 17    | MP990          | Unassigned                              |         |        | Reserved for future use.                                                                                                                                                                                                                                                     |
| 18    | MP991          | Unassigned                              |         |        | Reserved for future use.                                                                                                                                                                                                                                                     |
| 19    | MP992          | Unassigned                              |         |        | Reserved for future use.                                                                                                                                                                                                                                                     |
| 20    | MP993          | Unassigned                              |         |        | Reserved for future use.                                                                                                                                                                                                                                                     |
| 21    | MP994          | Unassigned                              |         |        | Reserved for future use.                                                                                                                                                                                                                                                     |

## B-1 Lookup Tables

### B-1.A Insurance Type

| Code |                       Description                        |
| ---- | -------------------------------------------------------- |
| 12   | Preferred Provider Organization (PPO)                    |
| 13   | Point of Service (POS)                                   |
| 14   | Exclusive Provider Organization (EPO)                    |
| 15   | Indemnity Insurance                                      |
| 16   | Health Maintenance Organization (HMO) Medicare Advantage |
| 17   | Dental Maintenance Organization (DMO)                    |
| CH   | Children’s Health Insurance Program (CHIP)               |
| CI   | Commercial Insurance Company                             |
| DN   | Dental                                                   |
| HM   | Health Maintenance Organization                          |
| HN   | HMO Medicare Risk/ Medicare Part C                       |
| MA   | Medicare Part A                                          |
| MB   | Medicare Part B                                          |
| MC   | Medicaid Fee For Service (FFS)                           |
| MD   | Medicare Part D                                          |
| MP   | Medicare Primary                                         |
| MO   | Medicaid Accountable Care Organization (ACO)             |
| QM   | Qualified Medicare Beneficiary                           |
| SP   | Medicare Supplemental (Medi-gap) plan                    |
| TV   | Title V                                                  |
| 99   | Other                                                    |

### B-1.B Coverage Level Code

| Code |                              Description                              |
| ---- | --------------------------------------------------------------------- |
| CHD  | Children Only                                                         |
| DEP  | Dependents Only                                                       |
| ECH  | Employee and Children                                                 |
| EPN  | Employee plus N where N equals the number of other covered dependents |
| ELF  | Employee and Life Partner                                             |
| EMP  | Employee Only                                                         |
| ESP  | Employee and Spouse                                                   |
| FAM  | Family                                                                |
| IND  | Individual                                                            |
| SPC  | Spouse and Children                                                   |
| SPO  | Spouse Only                                                           |

### B-1.C Relationship Codes

| Code |                     Description                     |
| ---- | --------------------------------------------------- |
| 01   | Spouse                                              |
| 04   | Grandfather or Grandmother                          |
| 05   | Grandson or Granddaughter                           |
| 07   | Nephew or Niece                                     |
| 10   | Foster Child                                        |
| 15   | Ward                                                |
| 17   | Stepson or Stepdaughter                             |
| 19   | Child                                               |
| 20   | Employee/Self                                       |
| 21   | Unknown                                             |
| 22   | Handicapped Dependent                               |
| 23   | Sponsored Dependent                                 |
| 24   | Dependent of a Minor Dependent                      |
| 29   | Significant Other                                   |
| 32   | Mother                                              |
| 33   | Father                                              |
| 36   | Emancipated Minor                                   |
| 39   | Organ Donor                                         |
| 40   | Cadaver Donor                                       |
| 41   | Injured Plaintiff                                   |
| 43   | Child Where Insured Has No Financial Responsibility |
| 53   | Life Partner                                        |
| 76   | Dependent                                           |

### B-1.D Race Codes

|  Code  |                Description                |
| ------ | ----------------------------------------- |
| R1     | American Indian/Alaska Native             |
| R2     | Asian                                     |
| R3     | Black/African American                    |
| R4     | Native Hawaiian or other Pacific Islander |
| R5     | White                                     |
| R9     | Other Race                                |
| UNKNOW | Unknown/Not Specified                     |

### B-1.E Ethnicity Codes

|  Code  |                Description                 |
| ------ | ------------------------------------------ |
| 2182-4 | Cuban                                      |
| 2184-0 | Dominican                                  |
| 2148-5 | Mexican, Mexican American, Chicano         |
| 2180-8 | Puerto Rican                               |
| 2161-8 | Salvadoran                                 |
| 2155-0 | Central American (not otherwise specified) |
| 2165-9 | South American (not otherwise specified)   |
| 2060-2 | African                                    |
| 2058-6 | African American                           |
| AMERCN | American                                   |
| 2028-9 | Asian                                      |
| 2029-7 | Asian Indian                               |
| BRAZIL | Brazilian                                  |
| 2033-9 | Cambodian                                  |
| CVERDN | Cape Verdean                               |
| CARIBI | Caribbean Island                           |
| 2034-7 | Chinese                                    |
| 2169-1 | Columbian                                  |
| 2108-9 | European                                   |
| 2036-2 | Filipino                                   |
| 2157-6 | Guatemalan                                 |
| 2071-9 | Haitian                                    |
| 2158-4 | Honduran                                   |
| 2039-6 | Japanese                                   |
| 2040-4 | Korean                                     |
| 2041-2 | Laotian                                    |
| 2118-8 | Middle Eastern                             |
| PORTUG | Portuguese                                 |
| RUSSIA | Russian                                    |
| EASTEU | Eastern European                           |
| 2047-9 | Vietnamese                                 |
| OTHER  | Other Ethnicity                            |
| UNKNOW | Unknown/Not Specified                      |

### B-1.F Discharge Status

Discharge Status codes are defined and maintained by the National Uniform
Billing Committee (NUBC).

### B-1.G Type of Bill

Type of bill codes are defined and maintained by the National Uniform Billing
Committee (NUBC).

### B-1.H Claim Status

| Code |                       Description                        |
| ---- | -------------------------------------------------------- |
| 01   | Processed as primary                                     |
| 02   | Processed as secondary                                   |
| 03   | Processed as tertiary                                    |
| 19   | Processed as primary, forwarded to additional payer(s)   |
| 20   | Processed as secondary, forwarded to additional payer(s) |
| 21   | Processed as tertiary, forwarded to additional payer(s)  |
| 22   | Reversal of previous payment                             |

### B-1.I Present on Admission Codes

| Code |                                           Description                                           |
| ---- | ----------------------------------------------------------------------------------------------- |
| 3    | Unknown                                                                                         |
| 1    | Exempt for POA reporting                                                                        |
| E    | Exempt for POA reporting                                                                        |
| N    | Diagnosis was not present at time of inpatient admission                                        |
| U    | Documentation insufficient to determine if condition was present at time of inpatient admission |
| W    | Clinically undetermined                                                                         |
| Y    | Diagnosis was present at time of inpatient admission                                            |

### B-1.J Dispense as Written Codes

| Code |                           Description                            |
| ---- | ---------------------------------------------------------------- |
| 0    | Not dispensed as written                                         |
| 1    | Physician dispense as written                                    |
| 2    | Member dispense as written                                       |
| 3    | Pharmacy dispense as written                                     |
| 4    | No generic available                                             |
| 5    | Brand dispensed as generic                                       |
| 6    | Override                                                         |
| 7    | Substitution not allowed - brand drug mandated by law            |
| 8    | Substitution allowed - generic drug not available in marketplace |
| 9    | Other                                                            |

### B-1.K Drug Unit of Measure

| Code |     Description     |
| ---- | ------------------- |
| EA   | Each                |
| F2   | International Units |
| GM   | Grams               |
| ML   | Milliliters         |
| MG   | Milligrams          |
| MEQ  | Milliequivalent     |
| MM   | Millimeter          |
| UG   | Microgram           |
| UU   | Unit                |
| OT   | Other               |
